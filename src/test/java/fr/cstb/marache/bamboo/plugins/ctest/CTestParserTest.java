package fr.cstb.bamboo.plugins.ctest;

import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import junit.framework.Assert;
import junit.framework.TestCase;

import java.io.InputStream;
import java.util.List;

public class CTestParserTest extends TestCase
{
    public void testReportFile() throws Exception
    {
        CTestParser parser = new CTestParser();

        InputStream is = getClass().getResourceAsStream("/Test.xml");

        parser.parse(is);

        Assert.assertEquals(1, parser.getFailedTests().size());
        Assert.assertEquals(51, parser.getPassedTests().size());

        TestResults failingTest = parser.getFailedTests().iterator().next();
        Assert.assertEquals("./tests/tickets/ticket1", failingTest.getClassName());
        Assert.assertEquals("ticket1_app", failingTest.getActualMethodName());
        Assert.assertEquals(TestState.FAILED, failingTest.getState());
        Assert.assertEquals(1, failingTest.getErrors().size());

//        TestCaseResultError error = failingTest.getErrors().get(0);
//
//        Assert.assertEquals("uncaught exception of unknown type\n            ", error.getContent());

        List<TestResults> successfulTests = Lists.newLinkedList(parser.getPassedTests());

        TestResults successfulTest = successfulTests.get(0);

        Assert.assertEquals("./tests/Derived/IfcAxis1Placement", successfulTest.getClassName());
        Assert.assertEquals("IfcAxis1Placement_app", successfulTest.getActualMethodName());
    }

    public void test_3_2_3_ReportFile() throws Exception
    {
        CTestParser parser = new CTestParser();

        InputStream is = getClass().getResourceAsStream("/cmake-3.2.3_Test.xml");

        parser.parse(is);

        Assert.assertEquals(1, parser.getPassedTests().size());

        TestResults passedTest = parser.getPassedTests().iterator().next();
        Assert.assertEquals("./simla/test", passedTest.getClassName());
        Assert.assertEquals("R001_pipe1-grlo-free", passedTest.getActualMethodName());
        Assert.assertEquals(TestState.SUCCESS, passedTest.getState());
        Assert.assertEquals(0, passedTest.getErrors().size());

    }

    public void test_3_3_1_ReportFile() throws Exception
    {
        CTestParser parser = new CTestParser();

        InputStream is = getClass().getResourceAsStream("/cmake-3.3.1_Test.xml");

        parser.parse(is);

        Assert.assertEquals(1, parser.getPassedTests().size());

        TestResults passedTest = parser.getPassedTests().iterator().next();
        Assert.assertEquals("./simla/test", passedTest.getClassName());
        Assert.assertEquals("R001_pipe1-grlo-free", passedTest.getActualMethodName());
        Assert.assertEquals(TestState.SUCCESS, passedTest.getState());
        Assert.assertEquals(0, passedTest.getErrors().size());
    }

    public void test_3_12_3_ReportFile() throws Exception
    {
        CTestParser parser = new CTestParser();

        InputStream is = getClass().getResourceAsStream("/cmake-3.12.3_Test.xml");

        parser.parse(is);

        Assert.assertEquals(22, parser.getPassedTests().size());
        Assert.assertEquals(1, parser.getFailedTests().size());

        TestResults passedTest = parser.getPassedTests().iterator().next();
        Assert.assertEquals("./tests/tests_bin/tests/config", passedTest.getClassName());
        Assert.assertEquals("test_config.cpp", passedTest.getActualMethodName());
        Assert.assertEquals(TestState.SUCCESS, passedTest.getState());
        Assert.assertEquals(0, passedTest.getErrors().size());

        TestResults failedTest = parser.getFailedTests().iterator().next();
        Assert.assertEquals("./tests/tests_bin/tests", failedTest.getClassName());
        Assert.assertEquals("test_counter.cpp", failedTest.getActualMethodName());
        Assert.assertEquals(TestState.FAILED, failedTest.getState());
        Assert.assertEquals(1, failedTest.getErrors().size());
    }

}
